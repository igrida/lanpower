# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "lanpower"

Gem::Specification.new do |s|
  s.name              = 'lanpower'
  s.version           = Lanpower::VERSION
  s.platform          = Gem::Platform::RUBY
  s.has_rdoc          = true
  s.extra_rdoc_files  = ["README.rdoc"]
  s.summary           = "Lan power utility for Grid'5000 (IPMI, RSA, etc...)"
  s.description       = s.summary
  s.author            = "Pascal Morillon"
  s.email             = "pascal.morillon@irisa.fr"
  s.homepage          = "https://helpdesk.grid5000.fr/redmine/projects/show/lanpower"

  s.add_development_dependency "rspec", ">= 1.2.9"
  s.add_development_dependency "rdoc"

  s.add_dependency "mixlib-cli", ">= 1.1.0"
  s.add_dependency "mixlib-log"
  s.add_dependency "json", ">= 1.2.0"
  s.add_dependency 'concurrent-ruby', '~> 1.0', '>= 1.0.5'

  s.bindir            = "bin"
  s.executables       = %w( lanpower planpower oarlanpower )
  s.require_path      = ["lib"]
  s.files             = %w( README.rdoc ) + Dir.glob("lib/**/*")
end
