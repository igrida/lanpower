class Lanpower
  VERSION = "0.9.0"
  CONFIG_FILE = "/etc/lanpower.yaml"
  VALID_ACTIONS = ['status', 'on', 'off', 'cycle', 'safety_cycle', 'reset']
end

