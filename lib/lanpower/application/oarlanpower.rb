require "lanpower"
require "lanpower/application"
require "lanpower/config"
require "lanpower/bmc"
require "lanpower/log"
require "lanpower/task"
require 'timeout'
require 'syslog'
require 'concurrent'

class Lanpower::Application::OARlanpower < Lanpower::Application

  option :help,
    :short        => "-h",
    :long         => "--help",
    :description  => "Show this message",
    :on           => :tail,
    :boolean      => true,
    :show_options => true,
    :exit         => 0

  option :config_file,
    :short        => "-c CONFIG",
    :long         => "--config-file CONFIG",
    :description  => "Set configuration file (default: #{Lanpower::CONFIG_FILE})",
    :default      => Lanpower::CONFIG_FILE,
    :proc         => nil

  option :turn,
    :long         => "--turn STATUS",
    :description  => "Power on/off",
    :required     => true,
    :in           => ['on', 'off'],
    :proc         => nil

  option :debug,
    :short        => "-d",
    :long         => "--debug",
    :description  => "Active debug mode",
    :boolean      => true,
    :proc         => nil

  option :version,
    :short        => "-v",
    :long         => "--version",
    :description  => "Show version",
    :boolean      => true,
    :proc         => lambda {|_v| puts "lanpower: #{Lanpower::VERSION}"},
    :exit         => 0

  option :disable_log_decorators,
    :long         => "--disable-log-decorators",
    :description  => "Do not decorate log outputs, print message followed by datetime",
    :boolean      => true,
    :default      => false

  def initialize
    super
    @nodes = Array.new
    @input_nodes = []
    @configNodes = {}
    Syslog.open('lanpower')
  end

  def run
    # use parse_options from Mixlib
    parse_options

    Lanpower::Log.init() if config[:debug]
    Lanpower::Log.level(config[:debug] ? :debug : :info)
    Lanpower::Log.formatter = Lanpower::Log::NoDecoratorFormatter if config[:disable_log_decorators]

    conf        = Lanpower::Config.new(config[:config_file])
    clusters    = conf.value["clusters"].keys
    nodes       = Array.new

    STDIN.each do |line|
      @input_nodes << line.chomp.split(".").first
    end

    @input_nodes.each do |h|
      cluster     = clusters.select { |c| !h.match(c).nil? }.first
      if !cluster.nil?
        clusterConfig = conf.value["clusters"][cluster]
        node       = Lanpower::Bmc.new(
          :host       => h + clusterConfig["suffix"],
          :bmc        => clusterConfig["bmc"],
          :user       => clusterConfig["user"],
          :password   => clusterConfig["password"]
        )
        @configNodes[node.host] = {}
        @configNodes[node.host][:powerStatusRetry] = clusterConfig["oarPowerStatusRetry"] || 5
        @configNodes[node.host][:powerStatusDelay] = clusterConfig["oarPowerStatusDelay"] || 10
        @configNodes[node.host][:softPowerOffDelay] = clusterConfig["oarSoftPowerOffDelay"] || 30
        @configNodes[node.host][:ignoreError] = clusterConfig["oarIgnoreError"] || false
      else
        raise ArgumentError, "Node #{h} doesn't match any clusters in configuration file"
      end
      nodes << node
    end

    case config[:turn]
    when 'on'
      results = wakeup(nodes)
    when 'off'
      results = poweroff(nodes)
    else
      puts "--turn must be on|off"
      exit 1
    end

    Lanpower::Task.synchronize

    results.each do |host, status|
      puts host + ": " + status
    end

    if not results.select { |k,v| v == "error" and @configNodes[k][:ignoreError] == false }.empty?
      Syslog.info("Some nodes are into error")
      exit 1
    end

  end

  private

  def softPowerOff(_nodes)
    @input_nodes.each do |node|
      softpoweroff_task = Lanpower::Task.new :desc => "Soft power off node #{node}"
      softpoweroff_task.define do
        result = %x[ssh -p 6667 -i /var/lib/oar/.ssh/id_rsa -o ServerAliveInterval=1 -o ServerAliveCountMax=1 oar@#{node} "oardodo nohup /sbin/poweroff &>/dev/null &"]
        puts result if config[:debug]
      end
      softpoweroff_task.schedule
    end
  end

  def poweroff(nodes)
    softPowerOff(nodes)
    results = {}
    nodes.each do |node|
      powerstatus_task = Lanpower::Task.new :desc => "Power status node #{node.host}"
      powerstatus_task.define do
        begin
          results[node.host] = node.send('status')
          if results[node.host] == 'on'
            if (powerstatus_task.retry_count >= (powerstatus_task.times || 100))
              poweroff_task = Lanpower::Task.new :desc => "Power off node #{node.host}"
              poweroff_task.define do
                begin
                  results[node.host] = node.send('off')
                  powerstatus_task.retryWithDelay(6, @configNodes[node.host][:powerStatusDelay])
                rescue => powerOffException
                  puts "IPMI power off on node #{node.host} failed" if config[:debug]
                  puts powerOffException if config[:debug]
                  poweroff_task.retryWithDelay(3, 10)
                  results[node.host] = 'error'
                end
              end
              poweroff_task.schedule
            else
              powerstatus_task.retryWithDelay(@configNodes[node.host][:powerStatusRetry], @configNodes[node.host][:powerStatusDelay])
            end
          end
          results[node.host]
        rescue => powerStatusException
          puts "IPMI power status on node #{node.host} failed" if config[:debug]
          puts powerStatusException if config[:debug]
          powerstatus_task.retryWithDelay(@configNodes[node.host][:powerStatusRetry], @configNodes[node.host][:powerStatusDelay])
          results[node.host] = 'error'
        end
      end
      powerstatus_task.scheduleWithDelay(@configNodes[node.host][:softPowerOffDelay])
    end
    results
  end

  def wakeup(nodes)
    results = {}
    nodes.each do |node|
      wakeup_task = Lanpower::Task.new :desc => "Power on node #{node.host}"
      wakeup_task.define do
        begin
          results[node.host] = node.send('on')
          powerstatus_task = Lanpower::Task.new :desc => "Power status node #{node.host}"
          powerstatus_task.define do
            begin
              results[node.host] = node.send('status')
              if results[node.host] == 'off'
                wakeup_task.retryWithDelay(2,5)
              end
              results[node.host]
            rescue => powerStatusException
              puts "IPMI power status on node #{node.host} failed" if config[:debug]
              puts powerStatusException if config[:debug]
              powerstatus_task.retryWithDelay(5, 10)
              results[node.host] = 'error'
            end
          end
          powerstatus_task.scheduleWithDelay(10)
        rescue => powerOnException
          puts "IMPI power on on node #{node.host} failed" if config[:debug]
          puts powerOnException if config[:debug]
          wakeup_task.retryWithDelay(3,10)
          results[node.host] = 'error'
        end
        results[node.host]
      end
      wakeup_task.schedule
    end
    results
  end

end

