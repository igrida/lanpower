#!/usr/bin/env ruby

require "yaml"
require "syslog"
require "lanpower"
require "lanpower/application"
require "lanpower/config"
require "lanpower/log"
require "lanpower/bmc"
require "lanpower/snmp"

class Lanpower::Application::Lanpower < Lanpower::Application

  option :debug,
    :short        => "-d",
    :long         => "--debug",
    :description  => "Active debug mode",
    :boolean      => true,
    :proc         => nil

  option :version,
    :short        => "-v",
    :long         => "--version",
    :description  => "Show Lanpower version",
    :boolean      => true,
    :proc         => lambda {|_v| puts "Lanpower: #{::Lanpower::VERSION}"},
    :exit         => 0

  option :help,
    :short        => "-h",
    :long         => "--help",
    :description  => "Show this message",
    :on           => :tail,
    :boolean      => true,
    :show_options => true,
    :exit         => 0

  option :state,
    :short        => "-s",
    :long         => "--state",
    :description  => "Show the power status of the node",
    :boolean      => true

  option :command,
    :short        => "-c ACTION",
    :long         => "--command ACTION",
    :description  => "Action to perform [on, off, cycle, safety_cycle, reset]"

  option :sleep,
    :long         => "--sleep SEC",
    :description  => "Set how many seconds to sleep in mode safety_cycle (override value in #{::Lanpower::CONFIG_FILE})"

  option :machine,
    :short        => "-m MACHINE",
    :long         => "--machine MACHINE",
    :description  => "Machine to work on",
    :proc         => nil #lambda { |v| puts "hello #{v}" }

  # Options nowait and nocheck are not usable, this is due to a bug in mixlib-cli.
  # When long options start with --no, they are negate.
  # https://github.com/chef/mixlib-cli/issues/48
  # https://intranet.grid5000.fr/bugzilla/show_bug.cgi?id=13673
  option :nowait,
    :short        => "-n",
    :long         => "--no-wait",
    :description  => "Do not wait the node to have the required power status",
    :boolean      => true,
    :default      => false

  option :nocheck,
    :long         => "--no-check",
    :description  => "Do not check the status before to do a power cycle/on",
    :boolean      => true,
    :default      => false

  option :config,
    :long         => "--config FILE",
    :description  => "Configuration file to use (default: #{::Lanpower::CONFIG_FILE})",
    :default      => ::Lanpower::CONFIG_FILE

  option :disable_log_decorators,
    :long         => "--disable-log-decorators",
    :description  => "Do not decorate log outputs, print message followed by datetime",
    :boolean      => true,
    :default      => false

  def initialize
      super
      @nodes = Array.new
      @clusters = Array.new
      @max_tries = 5
      Syslog.open('lanpower')
  end

  def run
    begin
      parse_options
      configure
      @clusters = @conf.value["clusters"].keys rescue []
      @nodes_keys = @conf.value["nodes"].keys rescue []
      ::Lanpower::Log.debug("lanpower launched")
      if !config[:machine].nil?
        node = getnode(config[:machine])

        if !node.nil?
          # Compatibility (before, no type meant "bmc")
          if !node.has_key?("type")
            node["type"] = "bmc"
          end
          case node["type"]
          when "bmc"
            mynode = ::Lanpower::Bmc.new(:host => node["host"], :bmc => node["bmc"], :user => node["user"], :password => node["password"])
          when "snmp"
            mynode = ::Lanpower::Snmp.new(:host => node["host"], :snmp_device => node["snmp_device"], :snmp_version => node["snmp_version"], :community => node["community"],
                                          :oid => node["oid"], :snmp_on => node["snmp_on"], :snmp_off => node["snmp_off"])
          end
          if config[:state] # status option
            status = mynode.status
            Syslog.info("#{node["host"]}: status #{status}")
            case status
            when /^on /, /^off /
              puts "#{node["host"]}: #{status}"
              exit 0
            else
              $stderr.puts "#{node["host"]}: #{status}"
              exit 2
            end

          else # command option
            case config[:command]
            when "on"
              mynode.on
              Syslog.info("#{node["host"]}: executed command power on")
            when "off"
              mynode.off
              Syslog.info("#{node["host"]}: executed command power off")
            when "cycle"
              unless config[:nocheck]
                status = mynode.status
                Syslog.info("#{node["host"]}: status #{status}")
                unless config[:sleep].nil?
                  ::Lanpower::Log.debug("Sleep #{config[:sleep]} seconds.")
                  sleep config[:sleep].to_i
                end
                if status == 'off'
                  mynode.on
                  Syslog.info("#{node["host"]}: executed command power on")
                else
                  mynode.cycle
                  Syslog.info("#{node["host"]}: executed command cycle")
                end
              else
                mynode.cycle
                Syslog.info("#{node["host"]}: executed command cycle")
              end
            when "safety_cycle"
              mynode.off
              sleep_time = config[:sleep].nil? ? node["sleep"] : config[:sleep]
              ::Lanpower::Log.debug("Sleep #{sleep_time} seconds.")
              sleep sleep_time.to_i
              mynode.on
              Syslog.info("#{node["host"]}: executed command safety cycle (sleep of #{sleep_time})")
            when "reset"
              unless config[:nocheck]
                status = mynode.status
                Syslog.info("#{node["host"]}: status #{status}")
                unless config[:sleep].nil?
                  ::Lanpower::Log.debug("Sleep #{config[:sleep]} seconds.")
                  sleep config[:sleep].to_i
                end
                if status == 'off'
                  mynode.on
                  Syslog.info("#{node["host"]}: executed command power on")
                else
                  mynode.reset
                  Syslog.info("#{node["host"]}: executed command reset")
                end
              else
                mynode.reset
                Syslog.info("#{node["host"]}: executed command reset")
              end
            end

            unless config[:nowait]
              nb_tries = 0
              target = config[:command] == "off" ? "off" : "on"
              delay = node["command_delay"].nil? ? 1 : node["command_delay"].to_i
              ::Lanpower::Log.debug("Waiting #{delay} sec before to get the status")
              sleep delay
              while mynode.status != target and nb_tries < @max_tries
                ::Lanpower::Log.debug("Waiting for status #{target}: try #{nb_tries + 1} with a command delay of #{delay}")
                nb_tries += 1
                sleep delay
              end
            end
          end
        else
          raise "The machine #{node["host"]} is not valid"
        end
      else
        raise "A machine should be specified in parameter (see --help)."
      end
    rescue RuntimeError => e
      $stderr.puts "Error: #{e}"
      Syslog.err("#{e}")
      exit 2
    end
  end


  def configure
    #Ilanpower::Log.init("/var/log/ilanpower.log")
    ::Lanpower::Log.init() if config[:debug]
    ::Lanpower::Log.level(config[:debug] ? :debug : :info)
    ::Lanpower::Log.formatter = ::Lanpower::Log::NoDecoratorFormatter if config[:disable_log_decorators]
    #Ilanpower::Log.level(:debug)
    @conf = ::Lanpower::Config.new(config[:config])
    if !config[:state] and config[:command].nil?
      raise "Either -s, --state or -c ,--command parameters should be used."
    elsif !config[:state] and !["on", "off", "cycle", "safety_cycle", "reset"].include? config[:command]
      raise "#{config[:command]} is not a valid action."
    end
  end

  def getnode(arg)
    shortname = arg.split('.')[0]
    # Node-specific configuration
    nodename = @nodes_keys.select { |n| shortname == n }.first
    if !nodename.nil?
      node = @conf.value["nodes"][nodename]
    else
      # Cluster-wide configuration
      cluster = @clusters.select { |c| !arg.match(c).nil? }.first
      if !cluster.nil?
        node = @conf.value["clusters"][cluster]
      else
        raise "No matching configuration found for #{arg}."
      end
    end
    # Determine hostname
    if node.has_key?("suffix")
      node["host"] = shortname + node["suffix"]
    else
      node["host"] = shortname
    end
    node
  end

end
