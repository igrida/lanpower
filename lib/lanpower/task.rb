require 'concurrent'
require 'lanpower/taskobserver'

class Lanpower::Task

  @@scheduled_tasks = []
  @@debug = false

  attr_accessor :tag, :desc
  attr_reader :after, :future, :proc, :retry_count, :times

  def initialize(options = {})
    authorized_options = [:tag, :after, :desc]
    @name = ""
    @after = nil
    @times = nil
    @retry_count = 0
    options.each do |k,v|
      self.instance_variable_set("@#{k}", v) if authorized_options.include? k
    end
  end

  def define(&block)
    @proc = block
  end

  def schedule
    @future = Concurrent::Future.new(&@proc)
    @@scheduled_tasks << self
    self.execute
  end

  def scheduleWithDelay(delay)
    @future = Concurrent::ScheduledTask.new(delay, &@proc)
    @@scheduled_tasks << self
    self.execute
  end

  def retry(times)

  end

  def retryWithDelay(times, delay)
    @times ||= times
    if @retry_count <= @times
      self.scheduleWithDelay(delay)
      @retry_count += 1
    end
  end

  def self.synchronize(delay = 1)
    while (count = self.futures.select { |x| x.state != :fulfilled and x.state != :rejected }.count) > 0
      puts "Tasks counts : #{count}, #{futures.map { |y| y.state }.inspect}" if @@debug
      sleep delay
    end
  end

  protected

  def execute
    observer = Lanpower::TaskObserver.new(self)
    @future.add_observer(observer)
    @future.execute
  end

  class << self
    def futures
      @@scheduled_tasks.map { |x| x.future }
    end
  end

end
