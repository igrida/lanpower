require "lanpower"
require "mixlib/log"

class Lanpower
  class Log
    NoDecoratorFormatter = Proc.new do |_severity, datetime, _progname, msg|
      "#{msg} ; at #{datetime}\n"
    end

    extend Mixlib::Log
  end
end
