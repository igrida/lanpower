class Lanpower::TaskObserver

  attr_reader :task

  def initialize(task, _debug = false)
    @task = task
  end

  def update(time, value, reason)
    puts "The task '#{self.task.desc}' updated at #{time} with value '#{value}', reason '#{reason}', retry '#{self.task.retry_count}'"
  end

end

