require "mixlib/cli"
require "lanpower"

class Lanpower::Application
  include Mixlib::CLI

  def initialize
    super

    trap("TERM") do
      Lanpower::Application.fatal!("SIGTERM received, stopping", 1)
    end

    trap("INT") do
      Lanpower::Application.fatal!("SIGINT received, stopping", 2)
    end

    trap("HUP") do
      Lanpower::Log.info("SIGHUP received, reconfiguring")
    end

  end

  def run

  end

  class << self
    def fatal!(msg, err = -1)
      STDERR.puts("FATAL: #{msg}")
      Process.exit err
    end

    def exit!(_msg, err = -1)
      Process.exit err
    end
  end

end
